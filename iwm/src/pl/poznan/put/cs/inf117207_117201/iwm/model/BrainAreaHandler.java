package pl.poznan.put.cs.inf117207_117201.iwm.model;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class BrainAreaHandler extends SAXHandler<BrainArea> {

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
		case "description":
			getObject().setDescription(getNodeContent());
			break;
		case "function":
			getObject().addFunction(getNodeContent());
			break;
		case "area":
			addObjectToList();
			break;
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		switch (qName) {
		case "area":
			BrainArea brainArea = new BrainArea();
			brainArea.setName(attributes.getValue("name"));
			brainArea.setLatinName(attributes.getValue("latinName"));
			brainArea.setId(Integer.parseInt(attributes.getValue("id")));
			setNewObject(brainArea);
			break;
		case "source":
			Source source = new Source(attributes.getValue("text"));
			String url = attributes.getValue("url");
			if (url != null)
				source.setURL(url);
			getObject().addSource(source);
			break;
		}
	}
}
