package pl.poznan.put.cs.inf117207_117201.iwm.model;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class MappedImage {

	private Image image;
	private Image map;
	private PixelReader pixelReader;

	public MappedImage(Image image, Image map) {
		this.image = image;
		this.map = map;
		
		pixelReader = this.map.getPixelReader();
	}
	
	public MappedImage() {}

	public Image getImage() {
		return image;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}
	
	public Image getMap() {
		return map;
	}
	
	public void setMap(Image map) {
		this.map = map;
		pixelReader = map.getPixelReader();
	}

	public int getAreaNumber(double normalizedX, double normalizedY) {
		int actualX = (int)(normalizedX * map.getWidth());
		int actualY = (int)(normalizedY * map.getHeight());
		Color pixelColor = pixelReader.getColor(actualX, actualY);
		return (int) Math.round(pixelColor.getRed() * 255.0f);
	}
	
}
