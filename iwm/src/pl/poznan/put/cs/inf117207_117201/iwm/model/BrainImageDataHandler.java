package pl.poznan.put.cs.inf117207_117201.iwm.model;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import javafx.scene.image.Image;
import pl.poznan.put.cs.inf117207_117201.iwm.Resource;

public class BrainImageDataHandler extends SAXHandler<BrainImageData> {

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equals("image"))
			addObjectToList();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		switch (qName) {
		case "image":
			setNewObject(new BrainImageData());
			break;
		case "visibleImage":
			String imagePath = attributes.getValue("file");
			Image image = new Image(Resource.getURL(imagePath).toExternalForm());
			getObject().setBrainImage(image);
			break;
		case "mapImage":
			String mapFilePath = attributes.getValue("file");
			Image map = new Image(Resource.getURL(mapFilePath).toExternalForm());
			getObject().setBrainMap(map);
			break;
		case "crossSection":
			String crossSectionFilePath = attributes.getValue("file");
			image = new Image(Resource.getURL(crossSectionFilePath).toExternalForm());
			getObject().setCrossSectionImage(image);
			break;
		case "crossSectionMap":
			String crossSectionMapFilePath = attributes.getValue("file");
			map = new Image(Resource.getURL(crossSectionMapFilePath).toExternalForm());
			getObject().setCrossSectionMap(map);
			break;
		}
	}
}
