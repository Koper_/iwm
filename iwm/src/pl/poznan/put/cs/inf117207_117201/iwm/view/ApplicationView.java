package pl.poznan.put.cs.inf117207_117201.iwm.view;

import java.util.HashMap;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class ApplicationView extends TabPane {

	public static final String BRAIN_TAB = "Mozg";
	public static final String DISEASE_TAB = "Choroby";
	
	private HashMap<String, Tab> tabs;
	
	public ApplicationView(BrainView brainView, DiseasesView diseasesView) {
		super();
		setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		
		Tab brainViewTab = new Tab(BRAIN_TAB, brainView);
		Tab diseasesViewTab = new Tab(DISEASE_TAB, diseasesView);
		tabs = new HashMap<>();
		tabs.put(DISEASE_TAB, diseasesViewTab);
		tabs.put(BRAIN_TAB, brainViewTab);
		getTabs().addAll(tabs.values());
	}

	public void changeTab(String tabName) {
		getSelectionModel().select(tabs.get(tabName));
	}
}
