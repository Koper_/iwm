package pl.poznan.put.cs.inf117207_117201.iwm.model;

import javafx.scene.image.Image;

public class BrainImageData {

	private MappedImage brainImage;
	private MappedImage crossSection;
	
	public MappedImage getBrainImage() {
		return brainImage;
	}
	
	public void setBrainImage(Image brainImage) {
		if (this.brainImage == null)
			this.brainImage = new MappedImage();
		this.brainImage.setImage(brainImage);
	}
	
	public void setBrainImage(MappedImage brainImage) {
		this.brainImage = brainImage;
	}
	
	public MappedImage getCrossSection() {
		return crossSection;
	}
	
	public void setCrossSection(MappedImage crossSection) {
		this.crossSection = crossSection;
	}

	public void setBrainMap(Image map) {
		if (this.brainImage == null)
			this.brainImage = new MappedImage();
		this.brainImage.setMap(map);
	}

	public void setCrossSectionImage(Image image) {
		if (this.crossSection == null)
			this.crossSection = new MappedImage();
		this.crossSection.setImage(image);
		
	}

	public void setCrossSectionMap(Image map) {
		if (this.crossSection == null)
			this.crossSection = new MappedImage();
		this.crossSection.setMap(map);
	}
	
}
