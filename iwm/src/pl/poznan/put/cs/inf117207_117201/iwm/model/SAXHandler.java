package pl.poznan.put.cs.inf117207_117201.iwm.model;

import java.util.LinkedList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public abstract class SAXHandler<T> extends DefaultHandler {

	private List<T> objectList;
	private T object;
	private StringBuffer nodeContent;
	
	public SAXHandler() {
		objectList = new LinkedList<>();
		nodeContent = new StringBuffer();
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		nodeContent.append(String.copyValueOf(ch, start, length).replaceAll("[\t\n]", " ").replaceAll("  ", " "));
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		nodeContent.setLength(0);
	}

	public List<T> getObjectList() {
		return objectList;
	}
	
	protected void addObjectToList() {
		objectList.add(object);
	}
	
	protected void setNewObject(T object) {
		this.object = object;
	}
	
	protected T getObject() {
		return object;
	}
	
	protected String getNodeContent() {
		return nodeContent.toString().trim();
	}
}
