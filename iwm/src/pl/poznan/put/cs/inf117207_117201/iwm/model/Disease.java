package pl.poznan.put.cs.inf117207_117201.iwm.model;

import java.util.LinkedList;
import java.util.List;

public class Disease {

	private String name;
	private String latinName;
	private String hImagePath;
	private String iImagePath;
	private List<String> otherImageFiles;
	private List<String> symptoms;
	private List<String> causes;
	private List<Source> bibliography;
	private String description;
	
	public Disease() {
		otherImageFiles = new LinkedList<>();
		symptoms = new LinkedList<>();
		causes = new LinkedList<>();
		bibliography = new LinkedList<>();
	}
	
	public void addImage(String file) {
		otherImageFiles.add(file);
	}
	
	public void addSource(Source source) {
		bibliography.add(source);
	}

	public List<String> getSymptoms() {
		return symptoms;
	}

	public List<String> getCauses() {
		return causes;
	}
	
	public List<Source> getBibliography() {
		return bibliography;
	}
	
	public void addCause(String cause) {
		causes.add(cause);
	}
	
	public void addSymptom(String symptom) {
		symptoms.add(symptom);
	}

	public String getName() {
		return name;
	}

	public String getLatinName() {
		return latinName;
	}

	public String gethImagePath() {
		return hImagePath;
	}

	public String getiImagePath() {
		return iImagePath;
	}

	public List<String> getImageFiles() {
		return otherImageFiles;
	}

	public String getDescription() {
		return description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLatinName(String latinName) {
		this.latinName = latinName;
	}

	public void sethImagePath(String hImagePath) {
		this.hImagePath = hImagePath;
	}

	public void setiImagePath(String iImagePath) {
		this.iImagePath = iImagePath;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
