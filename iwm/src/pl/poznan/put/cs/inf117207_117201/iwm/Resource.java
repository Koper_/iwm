package pl.poznan.put.cs.inf117207_117201.iwm;

import java.net.URL;

public class Resource {

	static public String getFile(String resource) {
		return Main.class.getResource(resource).getFile();
	}
	
	static public URL getURL(String resource) {
		return Main.class.getResource(resource);
	}
	
	static public String getFileExternalForm(String resource) {
		return Main.class.getResource(resource).toExternalForm();
	}
}
