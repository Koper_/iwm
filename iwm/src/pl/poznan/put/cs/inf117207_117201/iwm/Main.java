package pl.poznan.put.cs.inf117207_117201.iwm;

import java.net.MalformedURLException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import javafx.application.Application;
import javafx.stage.Stage;
import pl.poznan.put.cs.inf117207_117201.iwm.model.*;
import pl.poznan.put.cs.inf117207_117201.iwm.view.*;
import javafx.scene.Scene;
import javafx.scene.image.Image;

public class Main extends Application {

	static final String VERSION = "v0.3 alpha";
	
	public <T> List<T> loadFromXML(String resource, SAXHandler<T> handler) throws ParserConfigurationException, SAXException {
		List<T> list = null;
		SAXLoader<T> loader = new SAXLoader<>(Resource.getFile(resource), handler);
		list = loader.getObjectList();
		return list;
	}
	
	@Override
	public void start(Stage primaryStage) throws MalformedURLException {
		List<BrainImageData> brainImages = null;
		List<BrainArea> brainAreas = null;
		List<Disease> diseases = null;	
		
		try {
			brainImages = loadFromXML("model/brainImages.xml", new BrainImageDataHandler());
			brainAreas = loadFromXML("model/brainAreas.xml", new BrainAreaHandler());
			diseases = loadFromXML("model/diseases.xml", new DiseasesHandler());			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		DiseasesView diseasesView = new DiseasesView();
		BrainView brainView = new BrainView();
		ApplicationView applicationView = new ApplicationView(brainView, diseasesView);
		
		new BrainViewController(brainView, brainImages, brainAreas);
		new ApplicationController(applicationView, primaryStage);
		new DiseasesViewController(diseasesView, diseases);
		
		Scene scene = new Scene(applicationView);
		scene.getStylesheets().clear();
		scene.getStylesheets().add(Main.class.getResource("view/application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		
		primaryStage.setTitle("Interaktywny Atlas M�zgu " + VERSION);
		primaryStage.getIcons().add(new Image(Resource.getURL("resources/icons/icon_64.png").toExternalForm()));
		primaryStage.getIcons().add(new Image(Resource.getURL("resources/icons/icon_32.png").toExternalForm()));
		primaryStage.getIcons().add(new Image(Resource.getURL("resources/icons/icon_16.png").toExternalForm()));
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
