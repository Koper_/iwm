package pl.poznan.put.cs.inf117207_117201.iwm.model;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class DiseasesHandler extends SAXHandler<Disease> {

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
		case "disease":
			addObjectToList();
			break;
		case "description":
			getObject().setDescription(getNodeContent());
			break;
		case "symptom":
			getObject().addSymptom(getNodeContent());
			break;
		case "cause":
			getObject().addCause(getNodeContent());
			break;
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		switch (qName) {
			case "disease":
				Disease disease = new Disease();
				disease.setName(attributes.getValue("name"));
				if (attributes.getValue("latinName") != null)
					disease.setLatinName(attributes.getValue("latinName"));
				setNewObject(disease);
				break;
			case "hImage":
				getObject().sethImagePath(attributes.getValue("file"));
				break;
			case "iImage":
				getObject().setiImagePath(attributes.getValue("file"));
				break;
			case "image":
				getObject().addImage(attributes.getValue("file"));
				break;
			case "source":
				Source source = new Source(attributes.getValue("text"));
				String url = attributes.getValue("url");
				if (url != null)
					source.setURL(url);
				getObject().addSource(source);
				break;
		}
	}
}
