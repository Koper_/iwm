package pl.poznan.put.cs.inf117207_117201.iwm.view;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.TextAlignment;
import pl.poznan.put.cs.inf117207_117201.iwm.model.Source;

public class SourceView extends Label {

	private String URL;
	
	SourceView(Source source) {
		URL = source.getURL();
		String labelText = "\t- " + source.getText();
		if (source.getURL() != null)
			labelText += " [" + source.getURL() + "]";
				
		this.setText(labelText);
		setWrapText(true);
		setTextAlignment(TextAlignment.JUSTIFY);
		setMaxWidth(500);
		
		this.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (Desktop.isDesktopSupported()) {
					try {
						Desktop.getDesktop().browse(new URI(URL));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (URISyntaxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
		});
	}
	
}
