package pl.poznan.put.cs.inf117207_117201.iwm.model;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class SAXLoader<T> {

	List<T> objectList;
	
	public SAXLoader(String xmlFilePath, SAXHandler<T> handler) throws ParserConfigurationException, SAXException {
		this(new File(xmlFilePath), handler);
	}
	
	public SAXLoader(File xmlFile, SAXHandler<T> handler) throws ParserConfigurationException, SAXException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(true);
		SAXParser parser = factory.newSAXParser();
		try {
			parser.parse(xmlFile, handler);
		} catch (IOException e) {
			e.printStackTrace();
		}
		objectList = handler.getObjectList();
	}

	public List<T> getObjectList() {
		return objectList;
	}
}
