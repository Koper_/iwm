package pl.poznan.put.cs.inf117207_117201.iwm.model;

public class Source {

	private String text;
	private String URL;
	
	public Source(String text) {
		super();
		this.text = text;
	}
	
	public Source(String text, String URL) {
		super();
		this.text = text;
		this.URL = URL;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getURL() {
		return URL;
	}
	
	public void setURL(String uRL) {
		URL = uRL;
	}	
}
