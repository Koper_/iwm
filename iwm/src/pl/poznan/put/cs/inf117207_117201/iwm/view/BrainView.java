package pl.poznan.put.cs.inf117207_117201.iwm.view;

import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import pl.poznan.put.cs.inf117207_117201.iwm.Main;
import pl.poznan.put.cs.inf117207_117201.iwm.model.BrainArea;

public class BrainView extends BorderPane {

	public static final int CROSS_SECTION = 0;
	public static final int BRAIN_IMAGE = 1;
	
	private Button buttonLeft;
	private Button buttonRight;
	private ImageView brainImage;
	
	private final int LEFT_RIGHT_BUTTON_SIZE = 50;
	private RadioButton brainImageRadioButton;
	private RadioButton crossSectionRadioButton;

	public BrainView() {
		Image imageLeft = new Image(Main.class.getResource("resources/icons/left.png").toExternalForm());
		Image imageRight = new Image(Main.class.getResource("resources/icons/right.png").toExternalForm());
		
		ImageView imageViewLeft = new ImageView(imageLeft);
		imageViewLeft.setFitHeight(LEFT_RIGHT_BUTTON_SIZE);
		imageViewLeft.setFitWidth(LEFT_RIGHT_BUTTON_SIZE);
		
		ImageView imageViewRight = new ImageView(imageRight);
		imageViewRight.setFitHeight(LEFT_RIGHT_BUTTON_SIZE);
		imageViewRight.setFitWidth(LEFT_RIGHT_BUTTON_SIZE);
		
		buttonLeft = new Button("", imageViewLeft);
		buttonRight = new Button("", imageViewRight);
		brainImage = new ImageView();
		brainImage.setPreserveRatio(true);
		
		BorderPane imageBorderPane = new BorderPane();
		
		imageBorderPane.setLeft(buttonLeft);
		imageBorderPane.setCenter(new ImageViewPane(brainImage));
		imageBorderPane.setRight(buttonRight);
				
		setAlignment(buttonLeft, Pos.CENTER_LEFT);
		setAlignment(buttonRight, Pos.CENTER_RIGHT);
		
		setCenter(imageBorderPane);
		
		VBox vBox = new VBox();
		brainImageRadioButton = new RadioButton("Obraz mozgu");
		crossSectionRadioButton = new RadioButton("Przekroj mozgu");
		ToggleGroup toggleGroup = new ToggleGroup();
		crossSectionRadioButton.setToggleGroup(toggleGroup);
		brainImageRadioButton.setToggleGroup(toggleGroup);
		vBox.getChildren().addAll(brainImageRadioButton, crossSectionRadioButton);
		setBottom(vBox);
	}
	
	public void setImage(Image image) {
		brainImage.setImage(image);
	}
	
	public void addBrainImageRadioButtonListener(ChangeListener<? super Boolean> listener) {
		brainImageRadioButton.selectedProperty().addListener(listener);
	}
	
	public void addCrossSectionRadioButtonListner(ChangeListener<? super Boolean> listener) {
		crossSectionRadioButton.selectedProperty().addListener(listener);
	}
	
	public void setSelectedRadioButton(int selected) {
		switch (selected) {
		case BRAIN_IMAGE:
			brainImageRadioButton.selectedProperty().set(true);
			break;
		case CROSS_SECTION:
			crossSectionRadioButton.selectedProperty().set(true);
			break;
		default:
			throw new IllegalArgumentException();
		}
	}
	
	public void addBrainImageListener(EventHandler<MouseEvent> handler) {
		brainImage.addEventHandler(MouseEvent.MOUSE_CLICKED, handler);
	}
	
	public void addLeftButtonListener(EventHandler<MouseEvent> handler) {
		buttonLeft.addEventHandler(MouseEvent.MOUSE_CLICKED, handler);
	}
	
	public void addRightButtonListener(EventHandler<MouseEvent> handler) {
		buttonRight.addEventHandler(MouseEvent.MOUSE_CLICKED, handler);
	}

	public void setBrainAreaDescription(BrainArea brainArea) {
		setRight(new BrainAreaDescriptionView(brainArea));
	}

	public double getCurrentImageWidth() {
		return brainImage.getBoundsInLocal().getWidth();
	}

	public double getCurrentImageHeight() {
		return brainImage.getBoundsInLocal().getHeight();
	}

	public void setRadioButtonsActive(boolean active) {
		brainImageRadioButton.setDisable(!active);
		crossSectionRadioButton.setDisable(!active);
	}
}
