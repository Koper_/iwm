package pl.poznan.put.cs.inf117207_117201.iwm.view;

import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

public class DiseasesView extends VBox {

	private ChoiceBox<String> diseasesList;
	private ScrollPane scrollPane;

	public DiseasesView() {
		super();
		Label diseasesChooseLabel = new Label("Wybierz chorob� :");
		diseasesList = new ChoiceBox<>();
		scrollPane = new ScrollPane();
		getChildren().addAll(diseasesChooseLabel, diseasesList, scrollPane);
	}
	
	public void setDiseasesList(ObservableList<String> diseases) {
		diseasesList.setItems(diseases);
	}
	
	public void setDiseaseDescription(Node description) {
		scrollPane.setContent(description);
	}
	
	public void addOnSelectionChangeListener(ChangeListener<? super String> listener) {
		diseasesList.getSelectionModel().selectedItemProperty().addListener(listener);
	}
}
