package pl.poznan.put.cs.inf117207_117201.iwm.model;

import java.util.ArrayList;
import java.util.List;

public class BrainArea {
	
	private String name;
	private String latinName;
	private int id;
	private String description;
	private List<String> functions;
	private List<Source> bibliography;
	
	public BrainArea() {
		this.functions = new ArrayList<>();
		this.bibliography = new ArrayList<>();
	}
	
	public BrainArea(String name, int id, String description) {
		this();
		this.name = name;
		this.id = id;
		this.description = description;
	}
	
	public BrainArea(String name, String latinName, int id, String description) {
		this(name, id, description);
		this.latinName = latinName;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLatinName() {
		return latinName;
	}
	
	public void setLatinName(String latinName) {
		this.latinName = latinName;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<String> getFunctions() {
		return functions;
	}
	
	public void addFunction(String function) {
		functions.add(function);
	}
	
	public List<Source> getBibliography() {
		return bibliography;
	}
	
	public void addSource(Source source) {
		bibliography.add(source);
	}

}
