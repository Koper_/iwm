package pl.poznan.put.cs.inf117207_117201.iwm;

import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import pl.poznan.put.cs.inf117207_117201.iwm.model.*;
import pl.poznan.put.cs.inf117207_117201.iwm.view.BrainView;

public class BrainViewController {

	private BrainImageData currentImagesData;
	private MappedImage currentImage;
	private List<BrainArea> brainAreas;
	private List<BrainImageData> brainImageDataList;
	private BrainView brainView;
	
	public BrainViewController(BrainView brainView, List<BrainImageData> imageList, List<BrainArea> brainAreas) {
		this.brainView = brainView;
		this.brainImageDataList = imageList;
		this.brainAreas = brainAreas;
		
		brainView.addLeftButtonListener(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				currentImagesData = brainImageDataList.get((brainImageDataList.indexOf(currentImagesData) + 1) % brainImageDataList.size());
				updateImage(currentImagesData.getBrainImage());
				brainView.setSelectedRadioButton(BrainView.BRAIN_IMAGE);
				if (currentImagesData.getCrossSection() == null)
					brainView.setRadioButtonsActive(false);
				else
					brainView.setRadioButtonsActive(true);
			}
			
		});
		
		brainView.addRightButtonListener(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				currentImagesData = brainImageDataList.get((brainImageDataList.indexOf(currentImagesData) + brainImageDataList.size() - 1) % brainImageDataList.size());
				updateImage(currentImagesData.getBrainImage());
				brainView.setSelectedRadioButton(BrainView.BRAIN_IMAGE);
				if (currentImagesData.getCrossSection() == null)
					brainView.setRadioButtonsActive(false);
				else
					brainView.setRadioButtonsActive(true);
			}
			
		});
		
		brainView.addBrainImageListener(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				double x = event.getX();
				double y = event.getY();
				double imageWidth = brainView.getCurrentImageWidth();
				double imageHeight = brainView.getCurrentImageHeight();
				int areaId = currentImage.getAreaNumber(x / imageWidth, y / imageHeight);
				BrainArea brainArea = getBrainAreaById(areaId);
				if (brainArea != null) {
					brainView.setBrainAreaDescription(brainArea);
				}
			}
			
		});
		
		brainView.addBrainImageRadioButtonListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue == Boolean.TRUE)
					 updateImage(currentImagesData.getBrainImage());
			}

		});
		
		brainView.addCrossSectionRadioButtonListner(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue == Boolean.TRUE)
					updateImage(currentImagesData.getCrossSection());
			}

		});
		currentImagesData = brainImageDataList.get(0);
		updateImage(currentImagesData.getBrainImage());
	}
	
	private void updateImage(MappedImage image) {
		currentImage = image;
		brainView.setImage(currentImage.getImage());
	}

	private BrainArea getBrainAreaById(int areaId) {
		for (BrainArea area : brainAreas) {
			if (area.getId() == areaId)
				return area;
		}
		return null;
	}	
	
}
