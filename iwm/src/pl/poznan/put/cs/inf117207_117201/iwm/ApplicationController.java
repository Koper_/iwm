package pl.poznan.put.cs.inf117207_117201.iwm;

import javafx.stage.Stage;
import pl.poznan.put.cs.inf117207_117201.iwm.view.ApplicationView;

public class ApplicationController {

	private ApplicationView applicationView;
	private Stage stage;

	public ApplicationController(ApplicationView applicationView, Stage stage) {
		this.applicationView = applicationView;
		this.stage = stage;
		changeTab(ApplicationView.BRAIN_TAB);
	}
	
	public void resizeWindowToFit() {
		stage.sizeToScene();
	}
	
	public void changeTab(String tabName) {
		applicationView.changeTab(tabName);
	}
}
