package pl.poznan.put.cs.inf117207_117201.iwm;

import java.util.LinkedList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import pl.poznan.put.cs.inf117207_117201.iwm.model.Disease;
import pl.poznan.put.cs.inf117207_117201.iwm.view.DiseaseView;
import pl.poznan.put.cs.inf117207_117201.iwm.view.DiseasesView;

public class DiseasesViewController {

	private List<Disease> diseases;

	public DiseasesViewController(DiseasesView diseasesView, List<Disease> diseases) {
		this.diseases = diseases;
		List<String> diseasesNames = new LinkedList<>();
		for (Disease disease : diseases)
			diseasesNames.add(disease.getName());
		diseasesView.setDiseasesList(FXCollections.observableList(diseasesNames));
		diseasesView.addOnSelectionChangeListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				for (Disease disease : diseases) {
					if (disease.getName().equals(newValue))
						diseasesView.setDiseaseDescription(new DiseaseView(disease));
				}
			}

		});
	}

}
