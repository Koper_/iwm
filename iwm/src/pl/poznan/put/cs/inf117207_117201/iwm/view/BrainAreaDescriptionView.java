package pl.poznan.put.cs.inf117207_117201.iwm.view;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import pl.poznan.put.cs.inf117207_117201.iwm.model.BrainArea;
import pl.poznan.put.cs.inf117207_117201.iwm.model.Source;

public class BrainAreaDescriptionView extends VBox {
	
	public BrainAreaDescriptionView(BrainArea area) {
		Label name = new Label(area.getName());
		name.getStyleClass().add("title");
		this.getChildren().add(name);
		
		if (area.getLatinName() != null) {
			Label latinName = new Label("�ac. " + area.getLatinName());
			latinName.getStyleClass().add("italic");
			this.getChildren().add(latinName);
		}

		if (area.getDescription() != null && !area.getDescription().equals("")) {
			Label descriptionLabel = new Label("Opis");
			descriptionLabel.getStyleClass().add("title-small");
			this.getChildren().add(descriptionLabel);
			
			Label description = new Label();
			description.setText(area.getDescription());
			description.setWrapText(true);
			description.setTextAlignment(TextAlignment.JUSTIFY);
			description.setMaxWidth(400);
			this.getChildren().add(description);
		}
		
		if (!area.getFunctions().isEmpty()) {
			Label functionsLabel = new Label("Odpowiada za:");
			functionsLabel.getStyleClass().add("title-small");
			this.getChildren().add(functionsLabel);
			
			for (String function : area.getFunctions()) {
				Label functionLabel = new Label("\t- " + function);
				functionLabel.setWrapText(true);
				functionLabel.setTextAlignment(TextAlignment.JUSTIFY);
				functionLabel.setMaxWidth(400);
				this.getChildren().add(functionLabel);
			}
		}
		
		if (!area.getBibliography().isEmpty()) {
			Label bibliographyLabel = new Label("Bibliografia");
			bibliographyLabel.getStyleClass().add("title-small");
			this.getChildren().add(bibliographyLabel);
			
			for (Source source : area.getBibliography()) {
				this.getChildren().add(new SourceView(source));
			}
		}
	}
}
