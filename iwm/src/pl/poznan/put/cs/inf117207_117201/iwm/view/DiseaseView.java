package pl.poznan.put.cs.inf117207_117201.iwm.view;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import pl.poznan.put.cs.inf117207_117201.iwm.Resource;
import pl.poznan.put.cs.inf117207_117201.iwm.model.Disease;
import pl.poznan.put.cs.inf117207_117201.iwm.model.Source;

public class DiseaseView extends VBox {

	public DiseaseView(Disease disease) {
		ObservableList<Node> children = getChildren();
		
		Label diseaseName = new Label(disease.getName());
		diseaseName.getStyleClass().add("title");
		Label diseaseLatinName = null;
		
		if (disease.getLatinName() != null) {
			diseaseLatinName = new Label(disease.getLatinName());
			diseaseLatinName.getStyleClass().add("italic");
		}

		
		Label description = new Label(disease.getDescription());
		description.setWrapText(true);
		description.setTextAlignment(TextAlignment.JUSTIFY);
		description.setMaxWidth(500);
		
		children.add(diseaseName);
		
		if (diseaseLatinName != null)
			children.add(diseaseLatinName);

		Label descriptionLabel = new Label("Opis");
		descriptionLabel.getStyleClass().add("title-small");
		children.add(descriptionLabel);
		
		children.add(description);
				
		VBox iImage = imageViewWithLabel(Resource.getFileExternalForm(disease.getiImagePath()), "Chora tkanka");
		VBox hImage = imageViewWithLabel(Resource.getFileExternalForm(disease.gethImagePath()), "Zdrowa tkanka");
		
		HBox ihImages = new HBox();
		ihImages.getChildren().addAll(hImage, iImage);
		
		children.add(ihImages);
		
		Label causes = new Label("Przyczyny");
		causes.getStyleClass().add("title-small");
		children.add(causes);
		
		for (String cause : disease.getCauses()) {
			Label causeLabel = new Label("\t- " + cause);
			causeLabel.setWrapText(true);
			causeLabel.setTextAlignment(TextAlignment.JUSTIFY);
			causeLabel.setMaxWidth(500);
			children.add(causeLabel);
		}

		Label symptoms = new Label("Objawy");
		symptoms.getStyleClass().add("title-small");
		children.add(symptoms);
		
		for (String symptom : disease.getSymptoms()) {
			Label symptomLabel = new Label("\t- " + symptom);
			symptomLabel.setWrapText(true);
			symptomLabel.setTextAlignment(TextAlignment.JUSTIFY);
			symptomLabel.setMaxWidth(500);
			children.add(symptomLabel);
		}

		Label bibliography = new Label("Bibliografia");
		bibliography.getStyleClass().add("title-small");
		children.add(bibliography);
		
		for (Source source : disease.getBibliography()) {
			children.add(new SourceView(source));
		}
	}

	private VBox imageViewWithLabel(String imageFile, String labelText) {
		VBox vbox = new VBox();
		
		Label label = new Label(labelText);
		label.getStyleClass().add("italic");
		
		ImageView imageView = new ImageView(new Image(imageFile));
		imageView.setPreserveRatio(true);
		imageView.setFitWidth(250);
		
		vbox.getChildren().addAll(imageView, label);
		vbox.setAlignment(Pos.BOTTOM_CENTER);
		
		return vbox;
	}
	
}
